import json
from main import isVegan

def load_params_from_json('rozanna.json'):
    with open(rozanna.json) as rozanna:
        return json.load(rozanna)

def test_rozana_isVegan_true():
    rozana_data=load_params_from_json('rozana.json')
    assert isVegan(rozana_data) == True


